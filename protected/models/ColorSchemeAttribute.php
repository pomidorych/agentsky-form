<?php
/**
 * Created by PhpStorm.
 * User: A&A
 * Date: 4/21/16
 * Time: 3:21 PM
 */

class ColorSchemeAttribute {
    public $title;
    public $inputName;
    public $formClass;
    public $cssAttribute;
    public $value;

    public function __construct($title, $inputName, $formClass, $cssAttribute){
        $this->title = $title;
        $this->inputName = $inputName;
        $this->formClass = $formClass;
        $this->cssAttribute = $cssAttribute;
    }
}