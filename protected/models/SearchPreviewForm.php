<?php
/**
 * Created by PhpStorm.
 * User: A&A
 * Date: 4/15/16
 * Time: 4:45 PM
 * 
 */

class SearchPreviewForm extends CFormModel{

    public $id;
    /*DESIGN PARAMS*/
    public $colorSchemeIcon = "#CA4781";//#000
    public $colorSchemeBackground = "#fff";
    public $colorSchemeText = "#333"; //#000
    public $colorSchemeBorder = "#aaa";
    public $colorSchemeButton = "#CA4781";//#000
    public $colorSchemeButtonText = "#fff";
    public $colorSchemeInputBorder = "#CA4781";//#000
    public $width;
    public $is100per = false;
    public $borderRadius;

    /*OUTPUT PARAMS*/
    public $title;
    public $defaultOrigin;
    public $defaultDestination;
    public $language;
    public $currency;
    public $target;

    public function rules(){
        return [
            ["width, title, language, currency, target", "required"],
            ["is100per","boolean"],
            ["width", "numerical", "integerOnly" => true, "min" => 200],
            ["borderRadius", "numerical", "integerOnly" => true, "min" => 0],
            ["defaultOrigin, defaultDestination", "safe"]
        ];
    }

    public function attributeLabels(){
        return [
            "width" => "Width",
            "is100per" => "100% width",
        ];
    }
}