<?php
/**
 * Created by PhpStorm.
 * User: A&A
 * Date: 4/15/16
 * Time: 1:18 PM
 *
 *  @var $this FormController
 *  @var $model SearchForm
 *  @var $colorSchemeAttributes array
 */
$this->breadcrumbs=array(
	'Create Form',
);

$this->renderPartial("_form",[
    "model" => $model,
    "colorSchemeAttributes" => $colorSchemeAttributes
])
?>