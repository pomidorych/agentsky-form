<?php
/**
 * Created by PhpStorm.
 * User: A&A
 * Date: 4/18/16
 * Time: 5:20 PM
 *  @var $model SearchForm
 */
?>
<div class="form-preview-container">

<div id="form-container" class="resize formBackground formBorder" style="width: <?=$model->width?>px" onresize="resizeInsides(this)">
<form name="searchForm">
<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"> <a href="#roundTrip" aria-controls="profile" role="tab" data-toggle="tab" class=" navigation-tab formButton formButtonText  btn">Round Trip</a></li>
    <li role="presentation"><a href="#oneWay" aria-controls="home" role="tab" data-toggle="tab" class=" navigation-tab formText btn">One Way</a></li>
    <li role="presentation"><a href="#multipleCity" aria-controls="messages" role="tab" data-toggle="tab" class=" navigation-tab formText btn">Multiple City</a></li>
</ul>
<div class="tab-content">

    <!--ROUND TRIP-->
    <div role="tabpanel" class="tab-pane active" id="roundTrip">
        <div class=" row padding-0-10  padding-10">
            <div class="col-md-6 col-xs-12">
                <div class=" row padding-0-10 ">
                    <!--FROM-->
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group form-group-icon-left">
                            <label class="formText" for="fromInput">From</label>
                            <div class="input-group formInput">
                                <span class="input-group-addon" id="fromAddon"><i class="formIcon fa fa-map-marker"></i></span>
                                <input id="fromInput" type="text" class="form-control" placeholder="City or Airport" aria-describedby="fromAddon">
                            </div>
                        </div>
                    </div>
                    <!--TO-->
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group form-group-icon-left">
                            <label class="formText" for="toInput">To</label>
                            <div class="input-group formInput">
                                <span class="input-group-addon" id="toAddon"><i class="formIcon fa fa-map-marker"></i></span>
                                <input id="toInput" type="text" class="form-control  " placeholder="City or Airport" aria-describedby="toAddon">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="input-daterange" data-date-format="M d, D">
                    <div class=" row padding-0-10 ">
                        <!--DEPARTING-->
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group form-group-icon-left">
                                <label class="formText" for="departingInput">Departing</label>
                                <div class="input-group formInput">
                                    <span class="input-group-addon" id="departingAddon"><i class="formIcon fa fa-calendar"></i></span>
                                    <input id="departingInput" type="text" class="form-control " placeholder="Departure Date" aria-describedby="departingAddon">
                                </div>
                            </div>
                        </div>
                        <!--RETURNING-->
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group form-group-icon-left">
                                <label class="formText" for="returningInput">Returning</label>
                                <div class="input-group formInput">
                                    <span class="input-group-addon" id="returningAddon"><i class="formIcon fa fa-calendar"></i></span>
                                    <input id="returningInput" type="text" class="form-control" placeholder="Return Date" aria-describedby="returningAddon">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--ONE WAY-->
    <div role="tabpanel" class="tab-pane" id="oneWay">
        <div class=" row padding-0-10  padding-10">
            <div class="col-md-6 col-xs-12">
                <div class=" row padding-0-10 ">
                    <!--FROM-->
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group form-group-icon-left">
                            <label class="formText" for="fromInput">From</label>
                            <div class="input-group formInput">
                                <span class="input-group-addon" id="fromAddon"><i class="formIcon fa fa-map-marker"></i></span>
                                <input id="fromInput" type="text" class="form-control formInput " placeholder="City or Airport" aria-describedby="fromAddon">
                            </div>
                        </div>
                    </div>
                    <!--TO-->
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group form-group-icon-left">
                            <label class="formText" for="toInput">To</label>
                            <div class="input-group formInput">
                                <span class="input-group-addon" id="toAddon"><i class="formIcon fa fa-map-marker"></i></span>
                                <input id="toInput" type="text" class="form-control" placeholder="City or Airport" aria-describedby="toAddon">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="input-daterange" data-date-format="M d, D">
                    <div class=" row padding-0-10 ">
                        <!--DEPARTING-->
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group form-group-icon-left">
                                <label class="formText" for="departingInput">Departing</label>
                                <div class="input-group formInput">
                                    <span class="input-group-addon" id="departingAddon"><i class="formIcon fa fa-calendar"></i></span>
                                    <input id="departingInput" type="text" class="form-control" placeholder="Departure Date" aria-describedby="departingAddon">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--MULTIPLE CITY-->
    <div role="tabpanel" class="tab-pane" id="multipleCity">
        <div>
            <div class=" row padding-0-10  padding-0-10 margin-top-10">
                <!--FROM-->
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group form-group-icon-left">
                        <div class="input-group formInput">
                            <span class="input-group-addon" id="fromAddon"><i class="formIcon fa fa-map-marker"></i></span>
                            <input id="fromInput" type="text" class="form-control" placeholder="From City or Airport" aria-describedby="fromAddon">
                        </div>
                    </div>
                </div>
                <!--TO-->
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group form-group-icon-left">
                        <div class="input-group formInput">
                            <span class="input-group-addon" id="toAddon"><i class="formIcon fa fa-map-marker"></i></span>
                            <input id="toInput" type="text" class="form-control" placeholder="To City or Airport" aria-describedby="toAddon">
                        </div>
                    </div>
                </div>
                <!--DEPARTING-->
                <div class="col-xs-12 col-sm-3">
                    <div class="input-daterange" data-date-format="M d, D">
                        <div class="form-group form-group-icon-left">
                            <div class="input-group formInput">
                                <span class="input-group-addon" id="departingAddon"><i class="formIcon fa fa-calendar"></i></span>
                                <input id="departingInput" type="text" class="form-control" placeholder="Departure Date" aria-describedby="departingAddon">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=" row padding-0-10  padding-0-10">
                <!--FROM-->
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group form-group-icon-left">
                        <div class="input-group formInput">
                            <span class="input-group-addon" id="fromAddon"><i class="formIcon fa fa-map-marker"></i></span>
                            <input id="fromInput" type="text" class="form-control" placeholder="From City or Airport" aria-describedby="fromAddon">
                        </div>
                    </div>
                </div>
                <!--TO-->
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group form-group-icon-left">
                        <div class="input-group formInput">
                            <span class="input-group-addon" id="toAddon"><i class="formIcon fa fa-map-marker"></i></span>
                            <input id="toInput" type="text" class="form-control" placeholder="To City or Airport" aria-describedby="toAddon">
                        </div>
                    </div>
                </div>
                <!--DEPARTING-->
                <div class="col-xs-12 col-sm-3">
                    <div class="input-daterange" data-date-format="M d, D">
                        <div class="form-group form-group-icon-left">
                            <div class="input-group formInput">
                                <span class="input-group-addon" id="departingAddon"><i class="formIcon fa fa-calendar"></i></span>
                                <input id="departingInput" type="text" class="form-control" placeholder="Departure Date" aria-describedby="departingAddon">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <button class="btn-sm border-radius-0 btn-success btn formButton formButtonText " onclick="return false;">
                + Add flight
            </button>
        </div>
        <!--SEPARATOR-->
        <div class="clearfix">&nbsp;</div>
    </div>
</div>

<!--EXTRA OPTIONS-->
<div class=" row padding-0-10  padding-10">
    <div class="col-xs-12 col-md-6 col-xs-12">

        <div class=" row padding-0-10 ">

            <div class="col-xs-4">
                <div class="form-group">
                    <p data-toggle="tooltip" data-placement="left" title="12+ years" class="border-bottom-dashed margin-bottom-5 display-table-caption  formText">Adults</p>
                    <p class="passengerButton" onclick="passengerCalculation('adult', -1)"><i class="formIcon fa fa-minus"></i></p>
                    <span class="adult formText">1</span>
                    <p class="passengerButton" onclick="passengerCalculation('adult', 1)"><i class="formIcon fa fa-plus"></i></p>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="form-group">
                    <p data-toggle="tooltip" data-placement="top" title="2 - 11 years" class="border-bottom-dashed margin-bottom-5 display-table-caption formText">Children</p>
                    <a class="passengerButton" onclick="passengerCalculation('child', -1)"><i class="formIcon fa fa-minus"></i></a>
                    <span class="child formText">0</span>
                    <a class="passengerButton" onclick="passengerCalculation('child', 1)"><i class="formIcon fa fa-plus"></i></a>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="form-group">
                    <p data-toggle="tooltip" data-placement="top" title="under 2 years" class="border-bottom-dashed margin-bottom-5 display-table-caption formText">Infants</p>
                    <a class="passengerButton" onclick="passengerCalculation('infant', -1)"><i class="formIcon fa fa-minus"></i></a>
                    <span class="infant formText">0</span>
                    <a class="passengerButton" onclick="passengerCalculation('infant', 1)"><i class="formIcon fa fa-plus"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6 col-xs-12">
        <div class=" row padding-0-10 ">
            <div class="col-xs-6">
                <div class="form-group">
                    <label class="formText">Cabin Class</label>
                    <div class="formInput">
                        <select name="cabin" class="form-control">
                            <option value="0">All</option>
                            <option value="1">Economy</option>
                            <option value="2">Premium Economy</option>
                            <option value="3">Business</option>
                            <option value="4">First</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-xs-6">
                <div class="form-group">
                    <p class="formText margin-0">Flexible Dates</p>
                    <label class="formText">
                            <input class="i-check flex-dates" type="checkbox" data-id="flex">
                        +/-1 day
                    </label>
                </div>
            </div>
        </div>

    </div>


    <div class="col-xs-12">
        <button class="btn-lg border-radius-0 btn-info btn formButton formButtonText "><i class="fa fa-search"></i> Search </button>
    </div>
</div>

</form>
</div>


</div>