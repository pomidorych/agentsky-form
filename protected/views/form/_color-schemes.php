<?php
/**
 * Created by PhpStorm.
 * User: A&A
 * Date: 4/21/16
 * Time: 11:29 PM
 * @var $colorSchemeAttributes array
 * @var $colorSchemeAttribute ColorSchemeAttribute
 */
?>

<?php $firstColor = "#fff"; $secondColor = "#CA4781"?><!-- WHITE / DARK RED -->
<?php $colorSchemeAttributes["colorSchemeIcon"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeText"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeBorder"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeButton"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeButtonText"]->value = $firstColor;?>
<?php $colorSchemeAttributes["colorSchemeInputBorder"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeBackground"]->value = $firstColor;?>
<div class="color-scheme-icon" onclick="$('.color-scheme-icon').removeClass('active'); $(this).addClass('active');changeScheme(<?=str_replace('"', '\'', json_encode($colorSchemeAttributes))?>);" ><img src="/images/color-schemes/colorSchemeWhiteDarkred.png" /></div>

<?php $firstColor = "#fff"; $secondColor = "#000"?><!-- WHITE / BLACK -->
<?php $colorSchemeAttributes["colorSchemeIcon"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeText"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeBorder"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeButton"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeButtonText"]->value = $firstColor;?>
<?php $colorSchemeAttributes["colorSchemeInputBorder"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeBackground"]->value = $firstColor;?>
<div class="color-scheme-icon" onclick="$('.color-scheme-icon').removeClass('active'); $(this).addClass('active');changeScheme(<?=str_replace('"', '\'', json_encode($colorSchemeAttributes))?>);" ><img src="/images/color-schemes/colorSchemeWhiteBlack.png"/></div>

<?php $firstColor = "#fff"; $secondColor = "#0ebcf2"?><!-- WHITE / BLUE -->
<?php $colorSchemeAttributes["colorSchemeIcon"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeText"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeBorder"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeButton"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeButtonText"]->value = $firstColor;?>
<?php $colorSchemeAttributes["colorSchemeInputBorder"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeBackground"]->value = $firstColor;?>
<div class="color-scheme-icon" onclick="$('.color-scheme-icon').removeClass('active'); $(this).addClass('active');changeScheme(<?=str_replace('"', '\'', json_encode($colorSchemeAttributes))?>);" ><img src="/images/color-schemes/colorSchemeWhiteBlue.png" /></div>

<?php $firstColor = "#fff"; $secondColor = "#35a954"?><!-- WHITE / GREEN -->
<?php $colorSchemeAttributes["colorSchemeIcon"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeText"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeBorder"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeButton"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeButtonText"]->value = $firstColor;?>
<?php $colorSchemeAttributes["colorSchemeInputBorder"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeBackground"]->value = $firstColor;?>
<div class="color-scheme-icon" onclick="$('.color-scheme-icon').removeClass('active'); $(this).addClass('active');changeScheme(<?=str_replace('"', '\'', json_encode($colorSchemeAttributes))?>);" ><img src="/images/color-schemes/colorSchemeWhiteGreen.png" /></div>

<?php $firstColor = "#CA4781"; $secondColor = "#FFF"?><!-- DARK RED / WHITE -->
<?php $colorSchemeAttributes["colorSchemeIcon"]->value = $firstColor;?>
<?php $colorSchemeAttributes["colorSchemeText"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeBorder"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeButton"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeButtonText"]->value = $firstColor;?>
<?php $colorSchemeAttributes["colorSchemeInputBorder"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeBackground"]->value = $firstColor;?>
<div class="color-scheme-icon" onclick="$('.color-scheme-icon').removeClass('active'); $(this).addClass('active');changeScheme(<?=str_replace('"', '\'', json_encode($colorSchemeAttributes))?>);" ><img src="/images/color-schemes/colorSchemeDarkredWhite.png" /></div>

<?php $firstColor = "#000"; $secondColor = "#FFF"?><!-- BLACK / WHITE -->
<?php $colorSchemeAttributes["colorSchemeIcon"]->value = $firstColor;?>
<?php $colorSchemeAttributes["colorSchemeText"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeBorder"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeButton"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeButtonText"]->value = $firstColor;?>
<?php $colorSchemeAttributes["colorSchemeInputBorder"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeBackground"]->value = $firstColor;?>
<div class="color-scheme-icon" onclick="$('.color-scheme-icon').removeClass('active'); $(this).addClass('active');changeScheme(<?=str_replace('"', '\'', json_encode($colorSchemeAttributes))?>);" ><img src="/images/color-schemes/colorSchemeBlackWhite.png" /></div>

<?php $firstColor = "#0ebcf2"; $secondColor = "#fff"?><!-- BLUE / WHITE -->
<?php $colorSchemeAttributes["colorSchemeIcon"]->value = $firstColor;?>
<?php $colorSchemeAttributes["colorSchemeText"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeBorder"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeButton"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeButtonText"]->value = $firstColor;?>
<?php $colorSchemeAttributes["colorSchemeInputBorder"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeBackground"]->value = $firstColor;?>
<div class="color-scheme-icon" onclick="$('.color-scheme-icon').removeClass('active'); $(this).addClass('active');changeScheme(<?=str_replace('"', '\'', json_encode($colorSchemeAttributes))?>);" ><img src="/images/color-schemes/colorSchemeBlueWhite.png" /></div>

<?php $firstColor = "#35a954"; $secondColor = "#fff"?><!-- GREEN / WHITE -->
<?php $colorSchemeAttributes["colorSchemeIcon"]->value = $firstColor;?>
<?php $colorSchemeAttributes["colorSchemeText"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeBorder"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeButton"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeButtonText"]->value = $firstColor;?>
<?php $colorSchemeAttributes["colorSchemeInputBorder"]->value = $secondColor;?>
<?php $colorSchemeAttributes["colorSchemeBackground"]->value = $firstColor;?>
<div class="color-scheme-icon" onclick="$('.color-scheme-icon').removeClass('active'); $(this).addClass('active');changeScheme(<?=str_replace('"', '\'', json_encode($colorSchemeAttributes))?>);" ><img src="/images/color-schemes/colorSchemeGreenWhite.png" /></div>
