<?php
/* @var $this FormController */
/* @var $models array */
/* @var $model UserForm */

$this->breadcrumbs=array(
	'My Forms',
);
?>

<div id="getCodeModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content border-radius-0">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Copy this code to your web page</h4>
            </div>
            <div class="modal-body">
                <textarea class="col-xs-12 code-container"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn-sm border-radius-0 btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" onclick="copyToClipboard($(this).parent().parent().find('.code-container'))" class="btn-sm border-radius-0 btn btn-primary">Select all</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="row">
    <table class="col-xs-12 table table-responsive">
        <thead>
        <th>Title</th>
        <th>Language</th>
        <th>Currency</th>
        <th><!--controls--></th>
        </thead>
        <tbody>
            <?php foreach($models as $model):
                $data = json_decode($model->data, true);
                ?>
                <tr>
                    <td>
                        <p style="
                            color: <?=$data["colorSchemeText"]?>;
                            border: 1px solid <?=$data["colorSchemeBorder"]?>;
                            background: <?=$data["colorSchemeBackground"]?>;
                            padding: 7px;
                            margin: 0;
                            ">
                            <?=$model->title?>
                            <i style="color: <?=$data["colorSchemeIcon"]?>; background: #fff;" class="glyphicon glyphicon-plane float-right padding-2"></i>
                        </p>
                    </td>
                    <td><?=$data["language"]?></td>
                    <td><?=$data["currency"]?></td>
                    <td>
                        <button class="btn btn-default border-radius-0 margin-0-5" onclick="getSearchFormCode(<?=$model->id?>, 'modal')">Get code</button>
                        <?=CHtml::link("Edit", ["/form/update", "id" => $model->id], ["class"=>"btn btn-default border-radius-0 margin-0-5"])?>
                        <?=CHtml::link("Delete", ["/form/delete", "id" => $model->id], ["class"=>"btn btn-danger border-radius-0 margin-0-5", 'confirm' => 'Are you sure?'])?>
                    </td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>

    <div class="col-xs-12">
        <a href="<?=$this->createUrl("/form/create",[])?>">
            <button class="btn-lg border-radius-0 btn-info btn"> + Create a new form </button>
        </a>
    </div>
</div>




