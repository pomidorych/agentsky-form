<?php
/**
 * Created by PhpStorm.
 * User: A&A
 * Date: 4/15/16
 * Time: 1:18 PM
 *  @var $this FormController
 *  @var $model SearchForm
 *  @var $colorSchemeAttributes array
 *  @var $colorSchemeAttribute ColorSchemeAttribute
 */
?>
<div class="col-xs-12 form-container">


    <!--FORM PREVIEW START-->
    <?=$this->renderPartial("_form-preview", ["model" => $model])?>
    <!--FORM PREVIEW END-->

    <!--SEPARATOR-->
    <div class="clearfix">&nbsp;</div>

    <?= CHtml::beginForm();?>

    <!--ERRORS DISPLAY-->
    <?= CHtml::errorSummary($model);?>

    <!--SETTINGS START-->
    <div class="panel-group" id="" role="tablist" aria-multiselectable="true">

        <!--DESIGN START-->
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingDesign">
                <h4 class="panel-title">
                    <a role="button" onclick="$(this).find('i').toggleClass('glyphicon-triangle-bottom').toggleClass('glyphicon-triangle-top')" data-toggle="collapse" href="#collapseDesign" aria-expanded="true" aria-controls="collapseDesign" class="text-decoration-none">
                        <p class="margin-0">Design settings
                            <i class="glyphicon glyphicon-triangle-top float-right"></i>
                        </p>
                    </a>
                </h4>
            </div>
            <div id="collapseDesign" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingDesign">
                <div class="panel-body">
                    <!--WIDTH START-->
                    <div class="col-xs-12">
                        <?=CHtml::activeLabel($model, "width");?>
                        <?=CHtml::activeNumberField($model, "width", ["id" => "formWidth", "onchange" => 'setFormWidth(this.value)', "class" => ($model->is100per ? " display-none ": "display-block")]);?>
                        <?=CHtml::activeCheckBox($model, "is100per", ["onchange" => '$(this).parent().find("#formWidth").toggleClass("display-none"); if(this.checked){setFormWidth("100%");}']);?>
                        <?=CHtml::activeLabel($model, "is100per");?>
                    </div>
                    <!--WIDTH END-->

                    <!--SEPARATOR-->
                    <div class="clearfix">&nbsp;</div>

                    <!--COLOR SCHEME START-->
                    <div class="col-xs-12">
                        <div class="color-scheme-container">
                            <?php
                                foreach($colorSchemeAttributes as $k=>$colorSchemeAttribute){
                                    $colorSchemeAttribute->value = $model->{$colorSchemeAttribute->inputName};
                                    echo CHtml::activeHiddenField($model, $k,["id" => $k."Input"]);
                                }
                            ?>

                            <!--INITIAL COLOR SCHEME-->
                            <input type="hidden" class="initialColorScheme" onclick="changeScheme(<?=str_replace('"', '\'', json_encode($colorSchemeAttributes))?>)">


                            <!--COLOR SCHEMES-->
                            <?=$this->renderPartial("_color-schemes",["colorSchemeAttributes" => $colorSchemeAttributes])?>

                            <!--CUSTOM COLOR SCHEME-->
                            <div class="color-scheme-icon position-relative">
                                <div class="height-100per" onclick="$(this).parent().find('.custom-scheme').toggleClass('display-none');$('.color-scheme-icon').removeClass('active'); $(this).parent().addClass('active'); event.stopPropagation();">
                                    <div class="custom-scheme-label"><span>Custom scheme</span></div>
                                </div>

                                <?=$this->renderPartial("_custom-color-scheme",['model' => $model, "colorSchemeAttributes" => $colorSchemeAttributes])?>
                            </div>

                        </div>
                    </div>
                    <!--COLOR SCHEME END-->
                </div>
            </div>
        </div>
        <!--DESIGN END-->

        <!--OUTPUT SETTINGS START-->
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOutput">
                <h4 class="panel-title">
                    <a role="button" onclick="$(this).find('i').toggleClass('glyphicon-triangle-bottom').toggleClass('glyphicon-triangle-top')" data-toggle="collapse" href="#collapseOutput" aria-expanded="true" aria-controls="collapseOutput" class="text-decoration-none">
                        <p class="margin-0">Output settings
                            <i class="glyphicon glyphicon-triangle-bottom float-right"></i>
                        </p>
                    </a>
                </h4>
            </div>
            <div id="collapseOutput" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOutput">
                <div class="panel-body">
                    <div class="">
                        <div class="row">
                            <div class="col-sm-6">
                                <?=CHtml::activeLabel($model, "title");?>
                                <?=CHtml::activeTextField($model, "title", ['class' => "form-control"]);?>
                            </div>
                            <div class="col-sm-3">
                                <?=CHtml::activeLabel($model, "defaultOrigin");?>
                                <?=CHtml::activeTextField($model, "defaultOrigin", ['class' => "form-control"]);?>
                            </div>
                            <div class="col-sm-3">
                                <?=CHtml::activeLabel($model, "defaultDestination");?>
                                <?=CHtml::activeTextField($model, "defaultDestination", ['class' => "form-control"]);?>
                            </div>
                        </div>

                        <div class="clearfix">&nbsp;</div>

                        <div class="row">
                            <div class="col-sm-4">
                                <?=CHtml::activeLabel($model, "language");?>
                                <?=CHtml::activeDropDownList($model, "language", ['en' => "English", 'ru' => 'Russian'], ['class' => 'form-control']);?>
                            </div>
                            <div class="col-sm-4">
                                <?=CHtml::activeLabel($model, "currency");?>
                                <?=CHtml::activeDropDownList($model, "currency", [
                                    'USD' => "USD",
                                    'EUR' => "EUR",
                                    'GBP' => "GBP",
                                    'RUB' => "RUB",
                                    'UAH' => "UAH",
                                    'CNY' => "CNY",
                                ], ['class' => 'form-control']);?>
                            </div>

                            <div class="col-sm-4">
                                <?=CHtml::activeLabel($model, "target");?>
                                <?=CHtml::activeDropDownList($model, "target", ['_blank' => "Open in new window or tab", '_self' => 'Open in the same frame'], ['class' => 'form-control']);?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!--OUTPUT SETTINGS END-->
    </div>
    <!--SETTINGS END-->

    <!--SAVE BUTTON-->
    <div class="col-xs-12 padding-0">
        <?=CHtml::submitButton("Save the form and get the cod", ["class" => "btn-sm border-radius-0 btn-info btn", "name" => "submit"]);?>
    </div>

    <!--SEPARATOR-->
    <div class="clearfix">&nbsp;</div>

    <!--CODE TEXT AREA-->
    <div class="col-xs-12 padding-0 position-relative">
        <textarea class="col-xs-12 code-container"></textarea>
        <button onclick="copyToClipboard($(this).parent().find('.code-container')); return false;" class="border-radius-0 right-30 position-absolute btn btn-default btn-xs">Select all</button>
        <script type="text/javascript">
            $(document).ready(function(){
                getSearchFormCode(<?=$model->id?>, "textarea")
            });
        </script>
    </div>


    <?= CHtml::endForm();?>
</div>
