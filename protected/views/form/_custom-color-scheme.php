<?php
/**
 * Created by PhpStorm.
 * User: A&A
 * Date: 4/15/16
 * Time: 7:19 PM
 * @var $model SearchForm
 *  @var $colorSchemeAttributes array
 *  @var $colorSchemeAttribute ColorSchemeAttribute
 */
?>

<div class="display-none custom-scheme"><!--add class popup to hide on body click-->

    <h3>
        Custom color scheme
        <button class="btn btn-xs float-right" onclick="$(this).parent().parent().toggleClass('display-none'); return false;">X</button>
    </h3>
    <ul class="nav nav-pills nav-stacked col-md-4 custom-scheme-pills">
        <?php foreach ($colorSchemeAttributes as $colorSchemeAttribute):?>
            <li class="position-relative first-active">
                <a href="#<?=$colorSchemeAttribute->inputName?>Ref" data-toggle="pill"><?=$colorSchemeAttribute->title?></a>
                <div id="<?=$colorSchemeAttribute->inputName?>Preview" style="background-color: <?=$model->{$colorSchemeAttribute->inputName}?>"></div>
            </li>
        <?php endforeach;?>
    </ul>
    <div class="tab-content col-md-8">

        <?php foreach ($colorSchemeAttributes as $colorSchemeAttribute):?>
            <div class="tab-pane position-relative text-center first-active" id="<?=$colorSchemeAttribute->inputName?>Ref">
                <input id="<?=$colorSchemeAttribute->inputName?>Picker"
                    data-form-class="<?=$colorSchemeAttribute->formClass?>"
                    data-css-attribute="<?=$colorSchemeAttribute->cssAttribute?>"
                    data-preview="<?=$colorSchemeAttribute->inputName?>Preview"
                    data-input="<?=$colorSchemeAttribute->inputName?>Input"
                    class="colorPicker" value="<?=$model->{$colorSchemeAttribute->inputName} ?>"/>
            </div>
        <?php endforeach;?>



    </div>
</div>
