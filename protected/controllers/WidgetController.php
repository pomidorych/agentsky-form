<?php

class WidgetController extends Controller
{
	public function actionIndex()
	{
        header('Content-Type: application/javascript');

        $file = $this->getViewFile("_form");

        $string = '<form class="uk-form">';
            $string .='<ul class="nav nav-tabs" role="tablist">';
                $string .='<li class="active"> <a href="#roundTrip" class="formButton formButtonText  btn">Round Trip</a></li>';
                $string .='<li><a href="#oneWay" class="formText btn">One Way</a></li>';
                $string .='<li><a href="#multipleCity" class="formText btn">Multiple City</a></li>';
            $string .='</ul>';

            /*ROUND TRIP*/
            $string .= '<div role="tabpanel" class="tab-pane active" id="roundTrip">';
                $string .= '<div class="row padding-10">';
                    $string .= '<div class="col-md-6 col-xs-12">';
                        $string .= '<div class=" row padding-0-10 ">';
                            $string .= '<div class="col-md-6 col-xs-12">';
                                $string .= '<div class="form-group form-group-icon-left">';
                                    $string .= '<label class="formText" for="fromInput"></label>';
                                    $string .= '<div class="input-group formInput">';
                                        $string .= '<span class="input-group-addon" id="fromAddon">';
                                            $string .= '<i class="formIcon fa fa-map-marker"></i>';
                                        $string .= '</span>';
                                        $string .= '<input id="fromInput" type="text" class="form-control" placeholder="City or Airport" aria-describedby="fromAddon">';
                                    $string .= '</div>';
                                $string .= '</div>';
                            $string .= '</div>';
                            $string .= '<div class="col-md-6 col-xs-12">';
                                $string .= '<div class="form-group form-group-icon-left">';
                                    $string .= '<label class="formText" for="toInput"></label>';
                                    $string .= '<div class="input-group formInput">';
                                        $string .= '<span class="input-group-addon" id="toAddon">';
                                            $string .= '<i class="formIcon fa fa-map-marker"></i>';
                                        $string .= '</span>';
                                        $string .= '<input id="toInput" type="text" class="form-control  " placeholder="City or Airport" aria-describedby="toAddon">';
                                    $string .= '</div>';
                                $string .= '</div>';
                            $string .= '</div>';
                        $string .= '</div>';
                    $string .= '</div>';
                    $string .= '<div class="col-md-6 col-xs-12">';
                        $string .= '<div class="input-daterange" data-date-format="M d, D">';
                            $string .= '<div class=" row padding-0-10 ">';
                                $string .= '<div class="col-md-6 col-xs-12">';
                                    $string .= '<div class="form-group form-group-icon-left">';
                                        $string .= '<label class="formText" for="departingInput"></label>';
                                        $string .= '<div class="input-group formInput">';
                                            $string .= '<span class="input-group-addon" id="departingAddon">';
                                                $string .= '<i class="formIcon fa fa-calendar"></i>';
                                            $string .= '</span>';
                                            $string .= '<input id="departingInput" type="text" class="form-control " placeholder="Departure Date" aria-describedby="departingAddon">';
                                        $string .= '</div>';
                                    $string .= '</div>';
                                $string .= '</div>';
                                $string .= '<div class="col-md-6 col-xs-12">';
                                    $string .= '<div class="form-group form-group-icon-left">';
                                        $string .= '<label class="formText" for="returningInput"></label>';
                                        $string .= '<div class="input-group formInput">';
                                            $string .= '<span class="input-group-addon" id="returningAddon">';
                                                $string .= '<i class="formIcon fa fa-calendar"></i>';
                                            $string .= '</span>';
                                            $string .= '<input id="returningInput" type="text" class="form-control" placeholder="Return Date" aria-describedby="returningAddon">';
                                        $string .= '</div>';
                                    $string .= '</div>';
                                $string .= '</div>';
                            $string .= '</div>';
                        $string .= '</div>';
                    $string .= '</div>';
                $string .= '</div>';
            $string .= '</div>';

            /*ONE WAY*/
            $string .= '<div role="tabpanel" class="tab-pane" id="oneWay">';
                $string .= '<div class="row padding-10">';
                    $string .= '<div class="col-md-6 col-xs-12">';
                        $string .= '<div class="row padding-0-10 ">';
                            /*FROM*/
                            $string .= '<div class="col-md-6 col-xs-12">';
                                $string .= '<div class="form-group form-group-icon-left">';
                                    $string .= '<label class="formText" for="fromInput"></label>';
                                    $string .= '<div class="input-group formInput">';
                                        $string .= '<span class="input-group-addon" id="fromAddon">';
                                            $string .= '<i class="formIcon fa fa-map-marker"></i>';
                                        $string .= '</span>';
                                        $string .= '<input id="fromInput" type="text" class="form-control" placeholder="City or Airport" aria-describedby="fromAddon">';
                                    $string .= '</div>';
                                $string .= '</div>';
                            $string .= '</div>';
                            /*TO*/
                            $string .= '<div class="col-md-6 col-xs-12">';
                                $string .= '<div class="form-group form-group-icon-left">';
                                    $string .= '<label class="formText" for="toInput"></label>';
                                    $string .= '<div class="input-group formInput">';
                                        $string .= '<span class="input-group-addon" id="toAddon">';
                                            $string .= '<i class="formIcon fa fa-map-marker"></i>';
                                        $string .= '</span>';
                                        $string .= '<input id="toInput" type="text" class="form-control" placeholder="City or Airport" aria-describedby="toAddon">';
                                    $string .= '</div>';
                                $string .= '</div>';
                            $string .= '</div>';
                        $string .= '</div>';
                    $string .= '</div>';
                    $string .= '<div class="col-md-6 col-xs-12">';
                        $string .= '<div class="input-daterange" data-date-format="M d, D">';
                            $string .= '<div class=" row padding-0-10 ">';
                                /*DEPARTING*/
                                $string .= '<div class="col-md-6 col-xs-12">';
                                    $string .= '<div class="form-group form-group-icon-left">';
                                        $string .= '<label class="formText" for="departingInput"></label>';
                                        $string .= '<div class="input-group formInput">';
                                            $string .= '<span class="input-group-addon" id="departingAddon">';
                                                $string .= '<i class="formIcon fa fa-calendar"></i>';
                                            $string .= '</span>';
                                            $string .= '<input id="departingInput" type="text" class="form-control" placeholder="Departure Date" aria-describedby="departingAddon">';
                                        $string .= '</div>';
                                    $string .= '</div>';
                                $string .= '</div>';
                            $string .= '</div>';
                        $string .= '</div>';
                    $string .= '</div>';
                $string .= '</div>';
            $string .= '</div>';

            /*MULTIPLE CITY*/
            $string .= '<div role="tabpanel" class="tab-pane" id="multipleCity">';
                $string .= '<div class="row padding-10">';
                    /*FROM*/
                    $string .= '<div class="col-xs-12 col-sm-4">';
                        $string .= '<div class="form-group form-group-icon-left">';
                            $string .= '<div class="input-group formInput">';
                                $string .= '<span class="input-group-addon" id="fromAddon">';
                                    $string .= '<i class="formIcon fa fa-map-marker"></i>';
                                $string .= '</span>';
                                $string .= '<input id="fromInput" type="text" class="form-control" placeholder="From City or Airport" aria-describedby="fromAddon">';
                            $string .= '</div>';
                        $string .= '</div>';
                    $string .= '</div>';
                    /*TO*/
                    $string .= '<div class="col-xs-12 col-sm-4">';
                        $string .= '<div class="form-group form-group-icon-left">';
                            $string .= '<div class="input-group formInput">';
                                $string .= '<span class="input-group-addon" id="toAddon">';
                                    $string .= '<i class="formIcon fa fa-map-marker"></i>';
                                $string .= '</span>';
                                $string .= '<input id="toInput" type="text" class="form-control" placeholder="To City or Airport" aria-describedby="toAddon">';
                            $string .= '</div>';
                        $string .= '</div>';
                    $string .= '</div>';
                    /*DEPARTING*/
                    $string .= '<div class="col-xs-12 col-sm-3">';
                        $string .= '<div class="input-daterange" data-date-format="M d, D">';
                            $string .= '<div class="form-group form-group-icon-left">';
                                $string .= '<div class="input-group formInput">';
                                    $string .= '<span class="input-group-addon" id="departingAddon">';
                                        $string .= '<i class="formIcon fa fa-calendar"></i>';
                                    $string .= '</span>';
                                    $string .= '<input id="departingInput" type="text" class="form-control" placeholder="Departure Date" aria-describedby="departingAddon">';
                                $string .= '</div>';
                            $string .= '</div>';
                        $string .= '</div>';
                    $string .= '</div>';
                $string .= '</div>';
                $string .= '<div class=" row padding-10">';
                    /*FROM*/
                    $string .= '<div class="col-xs-12 col-sm-4">';
                        $string .= '<div class="form-group form-group-icon-left">';
                            $string .= '<div class="input-group formInput">';
                                $string .= '<span class="input-group-addon" id="fromAddon">';
                                    $string .= '<i class="formIcon fa fa-map-marker"></i>';
                                $string .= '</span>';
                                $string .= '<input id="fromInput" type="text" class="form-control" placeholder="From City or Airport" aria-describedby="fromAddon">';
                            $string .= '</div>';
                        $string .= '</div>';
                    $string .= '</div>';
                    /*TO*/
                    $string .= '<div class="col-xs-12 col-sm-4">';
                        $string .= '<div class="form-group form-group-icon-left">';
                            $string .= '<div class="input-group formInput">';
                                $string .= '<span class="input-group-addon" id="toAddon">';
                                    $string .= '<i class="formIcon fa fa-map-marker"></i>';
                                $string .= '</span>';
                                $string .= '<input id="toInput" type="text" class="form-control" placeholder="To City or Airport" aria-describedby="toAddon">';
                            $string .= '</div>';
                        $string .= '</div>';
                    $string .= '</div>';
                    /*DEPARTING*/
                    $string .= '<div class="col-xs-12 col-sm-3">';
                        $string .= '<div class="input-daterange" data-date-format="M d, D">';
                            $string .= '<div class="form-group form-group-icon-left">';
                                $string .= '<div class="input-group formInput">';
                                    $string .= '<span class="input-group-addon" id="departingAddon">';
                                        $string .= '<i class="formIcon fa fa-calendar"></i>';
                                    $string .= '</span>';
                                    $string .= '<input id="departingInput" type="text" class="form-control" placeholder="Departure Date" aria-describedby="departingAddon">';
                                $string .= '</div>';
                            $string .= '</div>';
                        $string .= '</div>';
                    $string .= '</div>';
                $string .= '</div>';
                $string .= '<div class="col-xs-12">';
                    $string .= '<button class="btn-sm border-radius-0 btn-success btn formButton formButtonText " onclick="return false;">+ Add flight</button>';
                $string .= '</div>';
            $string .= '</div>';

            /*EXTRA OPTIONS*/
            $string .= '<div class=" row padding-0-10  padding-10">';
                $string .= '<div class="col-xs-12 col-md-6 col-xs-12">';
                    $string .= '<div class=" row padding-0-10 ">';
                        $string .= '<div class="col-xs-4">';
                            $string .= '<div class="form-group">';
                                $string .= '<p data-toggle="tooltip" data-placement="left" title="12+ years" class="border-bottom-dashed margin-bottom-5 display-table-caption  formText">Adults</p>';
                                $string .= '<p class="passengerButton" onclick="passengerCalculation(-1)"><i class="formIcon fa fa-minus"></i></p>';
                                $string .= '<span class="adult formText">1</span>';
                                $string .= '<p class="passengerButton" onclick="passengerCalculation(1)"><i class="formIcon fa fa-plus"></i></p>';
                            $string .= '</div>';
                        $string .= '</div>';
                        $string .= '<div class="col-xs-4">';
                            $string .= '<div class="form-group">';
                                $string .= '<p data-toggle="tooltip" data-placement="top" title="2 - 11 years" class="border-bottom-dashed margin-bottom-5 display-table-caption formText">Children</p>';
                                $string .= '<a class="passengerButton" onclick="passengerCalculation(-1)"><i class="formIcon fa fa-minus"></i></a>';
                                $string .= '<span class="child formText">0</span>';
                                $string .= '<a class="passengerButton" onclick="passengerCalculation(1)"><i class="formIcon fa fa-plus"></i></a>';
                            $string .= '</div>';
                        $string .= '</div>';
                        $string .= '<div class="col-xs-4">';
                            $string .= '<div class="form-group">';
                                $string .= '<p data-toggle="tooltip" data-placement="top" title="under 2 years" class="border-bottom-dashed margin-bottom-5 display-table-caption formText">Infants</p>';
                                $string .= '<a class="passengerButton" onclick="passengerCalculation(-1)"><i class="formIcon fa fa-minus"></i></a>';
                                $string .= '<span class="infant formText">0</span>';
                                $string .= '<a class="passengerButton" onclick="passengerCalculation(1)"><i class="formIcon fa fa-plus"></i></a>';
                            $string .= '</div>';
                        $string .= '</div>';
                    $string .= '</div>';
                $string .= '</div>';
                $string .= '<div class="col-xs-12 col-md-6 col-xs-12">';
                    $string .= '<div class=" row padding-0-10 ">';
                        $string .= '<div class="col-xs-6">';
                            $string .= '<div class="form-group">';
                                $string .= '<label class="formText">Cabin Class</label>';
                                $string .= '<div class="formInput">';
                                    $string .= '<select name="cabin" class="form-control">';
                                        $string .= '<option value="0">All</option>';
                                        $string .= '<option value="1">Economy</option>';
                                        $string .= '<option value="2">Premium Economy</option>';
                                        $string .= '<option value="3">Business</option>';
                                        $string .= '<option value="4">First</option>';
                                    $string .= '</select>';
                                $string .= '</div>';
                            $string .= '</div>';
                        $string .= '</div>';
                        $string .= '<div class="col-xs-6">';
                            $string .= '<div class="form-group">';
                                $string .= '<p class="formText margin-0">Flexible Dates</p>';
                                $string .= '<label class="formText">';
                                    $string .= '<input class="i-check flex-dates" type="checkbox" data-id="flex">';
                                        $string .= '+/-1 day';
                                    $string .= '</label>';
                                $string .= '</div>';
                            $string .= '</div>';
                        $string .= '</div>';
                    $string .= '</div>';
                $string .= '<div class="col-xs-12">';
                    $string .= '<button class="btn-lg border-radius-0 btn-info btn formButton formButtonText ">';
                        $string .= '<i class="fa fa-search"></i>Search';
                    $string .= '</button>';
                $string .= '</div>';
            $string .= '</div>';

    $string .='</form>';



		$this->renderPartial('index',["form" => $string]);
	}

}