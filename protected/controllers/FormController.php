<?php

class FormController extends Controller
{
	public function actionIndex()
	{

        $models = UserForm::model()->findAll();

		$this->render('index', [
            "models" => $models
        ]);
	}

    public function actionCreate()
	{
        $model = new SearchPreviewForm();
        $colorSchemeAttributes = self::getColorSchemeAttributes();

        if(isset($_POST["SearchPreviewForm"])){

            $model->setAttributes($_POST['SearchPreviewForm'], false);

            if(!$model->validate()){
                $this->render('create', [
                    "model" => $model,
                    "colorSchemeAttributes" => $colorSchemeAttributes
                ]);
            }

            $userForm = new UserForm();
            $userForm->title = $model->title;
            $userForm->created_at = date("Y-m-d");
            $userForm->id_user = 1;
            $userForm->data = $json = json_encode($model);
            $userForm->save();

            /*redirect to update page*/
            $this->redirect(["/form/update","id" => $userForm->id]);
        }

		$this->render('create', [
            "model" => $model,
            "colorSchemeAttributes" => $colorSchemeAttributes
        ]);
	}

    public function actionUpdate($id)
	{
        $model = new SearchPreviewForm();
        $colorSchemeAttributes = self::getColorSchemeAttributes();

        if(!$userForm = UserForm::model()->findByPk($id)){
            $this->render('create', [
                "model" => $model
            ]);
        }

        if(isset($_POST["SearchPreviewForm"])){

            $model->setAttributes($_POST['SearchPreviewForm'], false);

            if(!$model->validate()){
                $this->render('create', [
                    "model" => $model,
                    "colorSchemeAttributes" => $colorSchemeAttributes
                ]);
            }

            $userForm->title = $model->title;
            $userForm->data = $json = json_encode($model);
            $userForm->save();
        }

        $model->setAttributes(json_decode($userForm->data, true), false);
        $model->id = $userForm->id;
        $model->width = $model->width + 2;

		$this->render('update', [
            "model" => $model,
            "colorSchemeAttributes" => $colorSchemeAttributes
        ]);
	}

    public function actionDelete($id)
	{

        //todo make POST request instead GET

        if(!isset($id)){
            $this->redirect(["/form/index"]);
        }
        if(!$userForm = UserForm::model()->findByPk((int)$id)){
            $this->redirect(["/form/index"]);
        }

        $userForm->delete();

        $this->redirect(["/form/index"]);
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/

    public static function getColorSchemeAttributes(){
        return [
            "colorSchemeIcon" => new ColorSchemeAttribute("Icons","colorSchemeIcon", "formIcon", "color"),
            "colorSchemeText" => new ColorSchemeAttribute("Text","colorSchemeText", "formText", "color"),
            "colorSchemeBackground" => new ColorSchemeAttribute("Background","colorSchemeBackground", "formBackground", "background-color"),
            "colorSchemeBorder" => new ColorSchemeAttribute("Border","colorSchemeBorder", "formBorder", "border-color"),
            "colorSchemeButton" => new ColorSchemeAttribute("Button","colorSchemeButton", "formButton", "background-color"),
            "colorSchemeButtonText" => new ColorSchemeAttribute("Button text","colorSchemeButtonText", "formButtonText", "color"),
            "colorSchemeInputBorder" => new ColorSchemeAttribute("Input border","colorSchemeInputBorder", "formInput", "border-color")
        ];
    }

    public function actionCode(){

        $id = (int)$_POST["id"];

        if(!$userForm = UserForm::model()->findByPk($id)){
            echo json_encode([
                "errors" => 1,
                "errorMessage" => "Form not found"
            ]);return;
        }


        $result = '<script>
        var hash = "xGTZPJpsLPvNHcm2B";
        (function(d, w) {
            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script");
            w[c] = w[c] || function() {
                (w[c].q = w[c].q || []).push(arguments);
            };
            s.async = true;
            s.src = (d.location.protocol === "https:" ? "https:": "http:")
                + "//localhost.agentsky.dev/js/SearchPreviewForm.js";
            n.parentNode.insertBefore(s, n);
        })(document, window, "AgentSky");
    </script>';

        $result = '<script type="text/javascript" src="http://'.$_SERVER["HTTP_HOST"].'/?r=widget"></script>';

        echo json_encode([
            "errors" => 0,
            "code" => $result
        ]);return;
    }
}