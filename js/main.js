/**
 * Created by A&A on 4/15/16.
 */
$(document).ready(function () {

    /*add active class to first items of collections*/
    $(".first-active").parent().find(".first-active:first").addClass("active");

    /* Hide all the popups */
    document.addEventListener("click", function(e){
        hidePopups(e);
    });

    /*init resizable div*/
    $( ".resize" ).resizable({
        handles: 'e'
    }).on( "resize", function( event, ui ) {
        $("#formWidth").val($(this).width());
    }).resize();

    /*init all the tooltips*/
    $('[data-toggle="tooltip"]').tooltip();

    /*custom scheme draggable*/
    $('.custom-scheme').draggable({containment: "body"});

    /*change navigation tabs style*/
    $('.navigation-tab').click(function(){
        var currentStyle = $(this).parent().parent().find('.active').find('.formButton').attr('style');
        console.log(currentStyle);
        $('.navigation-tab').removeClass("formButtonText").removeClass("formButton").addClass("formText").attr("style","");
        $(this).removeClass("formText").addClass("formButtonText").addClass("formButton").attr("style", currentStyle);

        /*clean the current style*/

    });

});

function hidePopups(e){
    $(".popup").each(function(){
        var clickedElem = e.srcElement;
        if (!$(clickedElem).parents('.popup').length) {
            $(this).addClass("display-none");
        }
    });
}

/*number of passenger calculation*/
function passengerCalculation(type, value){
    var adult = parseInt($('.adult').html()) + (type == 'adult' ? value : 0);
    var child = parseInt($('.child').html()) + (type == 'child' ? value : 0);
    var infant = parseInt($('.infant').html()) + (type == 'infant' ? value : 0);

    if(value < 0 && adult < infant){
        infant = adult;
    }

    if(
        (adult < 1) ||
        (child < 0) ||
        (infant < 0) ||
        (adult < infant) ||
        (adult+child+infant > 9)
        )
    { return false;}

    $('.adult').html(adult);
    $('.child').html(child);
    $('.infant').html(infant);

    return true;
}

function setFormWidth(value){
    $(".resize").css("width", value).resize();
}

function getSearchFormCode(id, target){
    $.ajax({
        url: "?r=form/code",
        method: "POST",
        data: {id: id},
        async: false
    }).done(function(data){

        var result = jQuery.parseJSON(data);
        if(result.error == 1){
            alert(result.errorMessage);
            return false;
        }

        if(target == "modal"){
            $("#getCodeModal").find(".code-container").html(htmlEntities(result.code));
            $("#getCodeModal").modal();
        }
        else{
            $('.code-container').html(htmlEntities(result.code));
        }

        return true;
    });
}

function copyToClipboard(elem){
    $(elem).select();
}

function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}
