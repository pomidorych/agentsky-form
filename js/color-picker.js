/**
 * Created by A&A on 4/18/16.
 */

$(document).ready(function(){

    /* Init color picker */
    $(".colorPicker").spectrum({
        flat: true,
        showInput: false,
        showAlpha: true,
        showInitial: false,
        showButtons: false,
        preferredFormat: "hex",
        move: function(color) {
            changeSchemeElements(this, color.toRgbString());
        }
    }).change(function(){
        changeSchemeElements(this, $(this).val());
    });

    /*load initial color scheme*/
    $(".initialColorScheme").click();

});

function changeScheme(scheme){
    Object.keys(scheme).forEach(function(key){
        $("#"+key+"Picker").val(scheme[key].value).trigger("change");
    });
}

function changeSchemeElements(elem, color){
    /*small preview*/
    var preview = $(elem).attr("data-preview");
    $("#"+preview).css("background-color", color);

    /*form preview*/
    var formPreview = $(elem).attr("data-form-class");/*class of the form attribute*/
    var cssAttribute = $(elem).attr("data-css-attribute");/*css attribute to change*/
    $("."+formPreview).css(cssAttribute, color);

    /*change input value*/
    var input = $(elem).attr("data-input");
    $("#"+input).val(color);
}
