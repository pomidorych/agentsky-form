/**
 * Created by A&A on 4/18/16.
 */

function resizeInsides(parent){
    var currentWidth = parent.offsetWidth;
    var elements = parent.getElementsByClassName('col-xs-12');

    for(var i = 0; i < elements.length; i++){
        if( currentWidth < 768 ){
            elements[i].style.width = "100%";
        }
        else{
            elements[i].style.width = "";
        }
    }

}